#! Encoding UTF-8
# 基础目录
INSTALL_PATH="/home/data/"
[[ -z ${SCRIPT_PATH} ]] && SCRIPT_PATH=$(cd $(dirname "$0") && pwd)

LIB_PATH="${SCRIPT_PATH}/library"
FUNCTION_PATH="${SCRIPT_PATH}/function"
TEMPLATE_PATH="${SCRIPT_PATH}/template"
DOWNLOAD_PATH="${SCRIPT_PATH}/download"
LOG_PATH="${SCRIPT_PATH}/log"

INFO_LOG=${LOG_PATH}/mlsbs_info_$(date +%F).log
ERR_LOG=${LOG_PATH}/mlsbs_err_$(date +%F).log

