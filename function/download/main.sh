#! Encoding UTF-8


select_download_fun(){
  mkdir -p ${DOWNLOAD_PATH}
  DOWNLOAD_SCRIPT_PATH=${FUNCTION_PATH}/download

  echo "----------------------------------------------------------------"
  declare -a VAR_LISTS
  if ${CN} ;then
    echo "[Notice] 请选择小功能:"
    VAR_LISTS=("返回首页" "下载'filebeat_oss'" "下载'node_exporter'" "下载'alertmanager'")
  else
    echo "[Notice] Which fun :"
    VAR_LISTS=("Back" "Download'filebeat_oss'" "Download'node_exporter'" "Download'alertmanager'")
  fi
  select VAR in ${VAR_LISTS[@]} ;do
    case ${VAR} in
      ${VAR_LISTS[1]})
        source ${DOWNLOAD_SCRIPT_PATH}/filebeat_oss_laster_linux-x64_download.sh ;;
      ${VAR_LISTS[2]})
        source ${DOWNLOAD_SCRIPT_PATH}/node_exporter_laster_linux-x64_download.sh ;;
      ${VAR_LISTS[3]})
        source ${DOWNLOAD_SCRIPT_PATH}/alertmanager_laster_linux-x64_download.sh ;;
      ${VAR_LISTS[0]})
        main;;
      *)
        select_download_fun;;
    esac
    break
  done
}

select_download_fun