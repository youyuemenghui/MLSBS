#! Encoding UTF-8
my_ip(){
  INTERNET_IP=$(timeout 10s curl ifconfig.me/ip)
	[[ -z "$(which ifconfig)" ]] \
    && INTERFACES_LISTS=($(ip link|awk -F':' '!/^ |lo/ {print $2}'|sed 's/^ //g')) \
    || INTERFACES_LISTS=($(ifconfig|awk '!/^ |^$|lo/ {print $1}'))
	[[ -z "${INTERFACES_LISTS}" ]] \
    && info_log "没有有效的网络配置" "No valid networks" 

  echo "++++++++++++++++++++++++++++++++++++++"
  echo "#            NETWORKS                #"
  echo "--------------------------------------"
  info "公网IP：${INTERNET_IP}" "INTER IP: ${INTERNET_IP}"
  TMP_COUNT=0
  for VAR in ${INTERFACES_LISTS[@]} ;do
    if [[ -z "$(which ifconfig)" ]];then
      IP_V4=$(ip addr show ${VAR}|awk '/inet / {print $2}')
      IP_V6=$(ip addr show ${VAR}|awk '/inet6/ {print $2}')
    else
      IP_V4=$(ifconfig -a ${VAR}|awk -F'[: ]+' '/inet addr/ {print $4}')
      IP_V6=$(ifconfig -a ${VAR}|awk '/inet6 addr/ {print $3}')
    fi
    echo -e "${INTERFACES_LISTS[${TMP_COUNT}]} :\n\t ipv4 = ${IP_V4} \n\t ipv6 = ${IP_V6}"
    echo "_______________________________"
    TMP_COUNT=$(expr ${TMP_COUNT} + 1)
  done
}